#!/bin/sh

# Welcome to the progressionlive laptop script!
# Be prepared to turn your laptop (or desktop, no haters here)
# into an awesome development machine.

fancy_echo() {
  local fmt="$1"; shift

  # shellcheck disable=SC2059
  printf "\\n$fmt\\n" "$@"
}

append_to_zshrc() {
  local text="$1" zshrc
  local skip_new_line="${2:-0}"

  if [ -w "$HOME/.zshrc.local" ]; then
    zshrc="$HOME/.zshrc.local"
  else
    zshrc="$HOME/.zshrc"
  fi

  if ! grep -Fqs "$text" "$zshrc"; then
    if [ "$skip_new_line" -eq 1 ]; then
      printf "%s\n" "$text" >> "$zshrc"
    else
      printf "\n%s\n" "$text" >> "$zshrc"
    fi
  fi
}

trap 'ret=$?; test $ret -ne 0 && printf "failed\n\n" >&2; exit $ret' EXIT

set -e

if ! command -v snap >/dev/null; then
    fancy_echo "Installing snap ..."
    sudo apt update
    sudo apt install snapd -yqq
    append_to_zshrc '# recommended by brew doctor'

    # shellcheck disable=SC2016
    append_to_zshrc 'export PATH=$PATH:/snap/bin' 1

    export PATH="/usr/local/bin:$PATH"
else
  fancy_echo "Snap already installed. Skipping ..."
fi

if ! command -v zsh >/dev/null; then
    fancy_echo "Installing zsh ..."

    sudo apt-get update
    sudo apt-get install zsh -yqq
else
  fancy_echo "Zsh already installed. Skipping ..."
fi

if ! command -v git >/dev/null; then
    fancy_echo "Installing git ..."

    sudo apt-get update
    sudo apt-get install git -yqq
else
  fancy_echo "Git already installed. Skipping ..."
fi


if [ ! -d "$HOME/.bin/" ]; then
  mkdir "$HOME/.bin"
fi

if [ ! -f "$HOME/.zshrc" ]; then
  touch "$HOME/.zshrc"
fi


case "$SHELL" in
  */zsh) : ;;
  *)
    fancy_echo "Changing your shell to zsh ..."
      chsh -s "$(which zsh)"
    ;;
esac

fancy_echo "Installing snap deamon..."
snap install snapd

fancy_echo "Installing vscode..."
snap install vscode

if ! command -v jetbrains-toolbox >/dev/null; then
  fancy_echo "Installing jetbrains-toolbox..."
  curl https://raw.githubusercontent.com/nagygergo/jetbrains-toolbox-install/master/jetbrains-toolbox.sh | bash
else
  fancy_echo "jetbrains-toolbox already installed. Skipping ..."
fi


fancy_echo "Installing intellij-idea-community..."
snap install intellij-idea-community

fancy_echo "Installing android-studio..."
snap install android-studio

fancy_echo "Installing datagrip..."
snap install datagrip

fancy_echo "Installing postman..."
snap install postman

fancy_echo "Installing docker..."
snap install docker

fancy_echo "Installing slack..."
snap install slack --classic

fancy_echo "Installing firefox..."
snap install firefox

fancy_echo "Installing libreoffice..."
snap install libreoffice

fancy_echo "Installing ngrok..."
snap install ngrok

fancy_echo "Installing tmux..."
snap install tmux --classic

fancy_echo "Installing  aws-cli..."
snap install aws-cli --classic

fancy_echo "Installing openjdk-8-jdk..."
## JAVA
sudo apt-get update
sudo apt-get install openjdk-8-jdk -yqq
