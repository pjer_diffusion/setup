Setup
======

Setup is a script to set up an linux(ubuntu, xubuntu, PopOs) laptop for our java/groovy/android development.

It can be run multiple times on the same machine safely.
It installs, upgrades, or skips packages
based on what is already installed on the machine.

Requirements
------------

We support:

* ubuntu 16.04
* ubuntu 18.04
* ubuntu 19.04
* PopOs 18.04
* PopOs 19.04

Older versions may work but aren't regularly tested.
Bug reports for older versions are welcome.

Install
-------

Download the script:

```sh
curl --remote-name https://bitbucket.org/gdesilets_diffusion/setup/raw/f996a452a6688780399e27da3d6aa10bbd7e5b20/linux.sh
```

Review the script (avoid running scripts you haven't read!):

```sh
less linux.sh
```

Execute the downloaded script:

```sh
./linux.sh 2>&1 | tee ~/linux.log
```

Optionally, review the log:

```sh
less ~/linux.log
```

Debugging
---------

Your last Setup run will be saved to `~/linux.log`.
Read through it to see if you can debug the issue yourself.
If not, copy the lines where the script failed OR attach the whole log file as an attachment into an email to gdesilets@diffusion.cc .

What it sets up
---------------

linux tools:

* [Snap] The app store for Linux.
* [Docker] A standardized unit of software.

[Snap]: https://snapcraft.io/
[Docker]: https://www.docker.com/

Unix tools:

* [Snapd] Background service that manages and maintains installed snaps
* [Git] for version control
* [Tmux] for saving project state and switching between projects
* [Zsh] as your shell
* [ngrok] Public URLs for...One command for an instant, secure URL to your localhost server through any NAT or firewall.
* [aws-cli] unified tool to manage your AWS services.

[Snapd]: https://snapcraft.io/snapd
[Git]: https://git-scm.com/
[Tmux]: http://tmux.github.io/
[Zsh]: http://www.zsh.org/
[ngrok]: https://ngrok.com/
[aws-cli]: https://ngrok.com/

Programming languages, package managers, and configuration:

* [Java] stable for writing general-purpose code

[Java]: https://openjdk.java.net/install/

Dev tools:

* [Datagrip] for storing relational data
* [VsCode] Code editing. Redefined. Free. Built on open source. Runs everywhere.
* [Android studio] Android Studio provides the fastest tools for building apps on every type of Android device.
* [Intellij Comunity] Enjoy productive Java
* [Firefox] Firefox Browser
* [Postman] The Collaboration Platform for API Development
* [slack] Chat your dreams
* [libreoffice] LibreOffice is a powerful and free office suite
* [jetbrain-toolbox] Manage your tools the easy way

[Datagrip]: https://www.jetbrains.com/datagrip/
[VsCode]: https://code.visualstudio.com/
[Intellij Comunity]: https://www.jetbrains.com/idea/
[Android studio]: https://developer.android.com/studio
[Firefox]: https://www.mozilla.org/fr/firefox/new/
[Postman]: https://www.getpostman.com/
[slack]: https://slack.com/intl/en-ca/
[libreoffice]: https://www.libreoffice.org/download/download/
[jetbrain-toolbox]: https://www.jetbrains.com/toolbox-app/

It should take less than 15 minutes to install (depends on your machine).

Contributing
------------

Edit the `linux.sh` file.
Document in the `README.md` file.
Follow shell style guidelines by using [ShellCheck].

```sh
sudo ap-get update
sudo apt-get install shellcheck
```

[ShellCheck]: http://www.shellcheck.net/about.html

Thank you, [contributors]!

By participating in this project,
you agree to abide by the progressionlive [code of conduct].

[code of conduct]: This is where the code of conduct would be if we had one

About ProgressionLive
----------------

![progressionlive](https://www.progressionlive.com/wp-content/uploads/2019/06/Logo-ProgressionLIVE-Inverse.png)

Setup is maintained by gabriel and progressionlive, inc.
The names and logos for progressionlive are trademarks of progressionlive, inc.

[hire]: https://www.progressionlive.com/en/careers/

